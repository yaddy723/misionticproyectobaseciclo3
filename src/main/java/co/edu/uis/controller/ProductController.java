package co.edu.uis.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import co.edu.uis.model.Product;
import co.edu.uis.model.User;
import co.edu.uis.repository.ProductRepository;
import co.edu.uis.service.UserService;
import java.security.Principal;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(path = "/product")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private UserService userService;

    @GetMapping("/all")
    public String listProducts(Model model,Principal principal) {
        String username = principal.getName();
        User currentUser = userService.getUser(username);
        List<Product> products = productRepository.findByUser(currentUser, Sort.by(Sort.Direction.ASC, "createdAt"));
        model.addAttribute("products", products);
        return "product/all";
    }
    @GetMapping("/new")
    public String newProduct(Model model,Principal principal) {
        String username = principal.getName();
        User currentUser = userService.getUser(username);
        Product p = new Product();
        p.setUser(currentUser);
        model.addAttribute("product", p);
        model.addAttribute("isNew", true);
        return "product/product";
    }
    @PostMapping("/new")
    public String saveProduct(@ModelAttribute Product product,Principal principal, RedirectAttributes redirAttrs, BindingResult bindingResult) {
        String username = principal.getName();
        User currentUser = userService.getUser(username);
        if (product == null){
            redirAttrs.addFlashAttribute("message", "Empty product");
            return "redirect/product/all";
        }
        if (bindingResult.hasErrors()) {
            redirAttrs.addFlashAttribute("message", bindingResult.getFieldErrors().toString());
	} else {
            
            redirAttrs.addFlashAttribute("message", "Product created");
        }
        product.setUser(currentUser);
        productRepository.save(product);

        return "redirect:/product/all";
    }
    @GetMapping("/{id}")
    public String getProduct(@PathVariable("id") Integer id,Model model,Principal principal) {
        String username = principal.getName();
        User currentUser = userService.getUser(username);
        Product product = productRepository.findByUserAndId(currentUser, id);
        model.addAttribute("product", product);
        model.addAttribute("isNew", false);
        return "product/product";
    }
    @GetMapping("/{id}/remove")
    public String removeProduct(@PathVariable("id") Integer id, Principal principal,RedirectAttributes redirAttrs) {
        String username = principal.getName();
        User currentUser = userService.getUser(username);
        Product product = productRepository.findByUserAndId(currentUser, id);
        if (product == null){
            redirAttrs.addFlashAttribute("message", "Product not found");
            return "redirect/product/all";
        }
        productRepository.delete(product);
        return "redirect:/product/all";
    }
    @PostMapping("/{id}")
    public String updateProduct(@PathVariable("id") Integer id,@ModelAttribute Product productUpdate,Principal principal, RedirectAttributes redirAttrs, BindingResult bindingResult) {
        String username = principal.getName();
        User currentUser = userService.getUser(username);
        Product product = productRepository.findByUserAndId(currentUser, id);
        if (product == null){
            redirAttrs.addFlashAttribute("message", "Product not found");
            return "redirect/product/all";
        }
        if (bindingResult.hasErrors()) {
            redirAttrs.addFlashAttribute("message", bindingResult.getFieldErrors().toString());
	} else {
            
            redirAttrs.addFlashAttribute("message", "Product updated");
        }
        productUpdate.setUser(currentUser);
        productRepository.save(productUpdate);

        return "redirect:/product/all";
    }


}
